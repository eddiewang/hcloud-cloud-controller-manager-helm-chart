# Helm Chart for hetzner-cloud-controller-manager

This is a community Helm Chart for installing the hcloud-cloud-controller-manager in your Hetzner Cloud Kubernetes cluster.
The sources of the hcloud-cloud-controller-manager can be found at https://github.com/hetznercloud/hcloud-cloud-controller-manager.

## Installation

### Add Helm Repository

```
helm repo add mlohr https://helm-charts.mlohr.com/
helm repo update
```

### Quick Install
```
helm install hcloud-cloud-controller-manager mlohr/hcloud-cloud-controller-manager --set hcloud.token=<HCLOUD API TOKEN>
```


## Configuration Parameters

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `hcloud.token` | Token for Hetzner Cloud API | `nil` |
| `hcloud.networks.enabled` | Enable the use of private networks | `false` |
| `hcloud.networks.id` | ID or name of internal network (find with `hcloud network list`) | `nil` |
| `hcloud.networks.clusterCidr` | Subnet used for internal server IPs | `10.233.252.0/22` |
| `hcloud.manager.image` | Image to be used for the manager | `hetznercloud/hcloud-cloud-controller-manager:v1.6.1` |
| `hcloud.manager.imagePullPolicy` | ImagePullPolicy | `IfNotPresent` |


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/bdtsim/-/blob/master/LICENSE.md) for more information.

Copyright (c) by Matthias Lohr <mail@mlohr.com>
